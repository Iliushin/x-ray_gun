#include "MicrobeamPhantomHit.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4LogicalVolume.hh"

G4Allocator<MicrobeamPhantomHit> MicrobeamPhantomHitAllocator;

MicrobeamPhantomHit::MicrobeamPhantomHit(G4LogicalVolume* logVol,G4int XID,G4int YID,G4int ZID)
    :logicalVolume(logVol),xHitPosition(XID),zHitPosition(ZID),yHitPosition(YID)

{
    energyDeposit = 0;
}

MicrobeamPhantomHit::~MicrobeamPhantomHit()
{
}
MicrobeamPhantomHit::MicrobeamPhantomHit(const MicrobeamPhantomHit&right): G4VHit()
{
    xHitPosition = right.xHitPosition;
    zHitPosition = right.zHitPosition;
    yHitPosition = right.yHitPosition;
    energyDeposit = right.energyDeposit;
    hitPosition = right.hitPosition;
    rotation = right.rotation;
    logicalVolume = right.logicalVolume;
}
const MicrobeamPhantomHit& MicrobeamPhantomHit::operator=(const MicrobeamPhantomHit &right)
{
    xHitPosition = right.xHitPosition;
    zHitPosition = right.zHitPosition;
    yHitPosition = right.yHitPosition;
    energyDeposit = right.energyDeposit;
    hitPosition = right.hitPosition;
    rotation = right.rotation;
    logicalVolume = right.logicalVolume;
    return *this;
}

int MicrobeamPhantomHit::operator==(const MicrobeamPhantomHit &right)
const
{
return((xHitPosition==right.xHitPosition)&&(zHitPosition==right.zHitPosition)&&(yHitPosition==right.yHitPosition));
}

void MicrobeamPhantomHit::Draw()
{
}

void MicrobeamPhantomHit::Print()
{
}
