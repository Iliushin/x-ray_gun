#include "MicrobeamRunAction.hh"
#include "MicrobeamEventAction.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "MicrobeamFactory.hh"
#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4SDManager.hh"
#include "G4Timer.hh"

MicrobeamRunAction::MicrobeamRunAction()
{}
MicrobeamRunAction::~MicrobeamRunAction()
{}
void MicrobeamRunAction::BeginOfRunAction(const G4Run* aRun)
{
    G4cout << "## Run " << aRun -> GetRunID() << " start." << G4endl;
}
void MicrobeamRunAction::SelectMonoEnergy(G4double value)
{
    (( MicrobeamFactory *) factory)->SelectMonoEnergy(value);
}
void MicrobeamRunAction::EndOfRunAction(const G4Run* aRun)
{
    G4cout << "number of event=" << aRun->GetNumberOfEvent() << G4endl;
}








