#include "MicrobeamPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ParticleTable.hh"
#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4UImanager.hh"
#include "G4RunManager.hh"
#include "MicrobeamFactory.hh"
#include "G4SystemOfUnits.hh"

void MicrobeamPrimaryGeneratorAction::SelectMonoEnergy(G4double value)
{
    if( value <0)
    {
        spectrumChoice=0;
    }
    else
    {
        spectrumChoice=1;
        MonoEnergy=value;
    }
}
G4double MicrobeamPrimaryGeneratorAction::MonoEnergy = -1 ;
G4int MicrobeamPrimaryGeneratorAction::spectrumChoice =0 ;
MicrobeamPrimaryGeneratorAction::MicrobeamPrimaryGeneratorAction(G4double sizeZ)
{
    primaryMessenger = new MicrobeamPrimaryGeneratorMessenger(this);
    factory = new MicrobeamFactory();
    G4int numberParticles = 1;
    this->sizeZ=sizeZ/2;
    particleGun = new G4ParticleGun(numberParticles);
    // Gamma energy spectrum ...
    G4double data[] =
    {
        //!!! Here all the data relative to the ESRF filtered spectrum are included, but not reported in this appendix!!!
    };
    NbeamEne=191;
    Xene= new G4double [NbeamEne];
    YprobInteg= new G4double [NbeamEne];
    G4double oldY = -data[1];
    G4double oldX=data[0];
    G4double sum=0.0;
    for(G4int i=0; i<NbeamEne;i++)
    {
        Xene[i] = data[2*i] ;
        sum=sum + 0.5*(oldY+data[2*i+1] )*(data[2*i]-oldX);
        YprobInteg[i]=sum;
        oldX=data[2*i];
        oldY=data[2*i+1];
    }
}
MicrobeamPrimaryGeneratorAction::~MicrobeamPrimaryGeneratorAction()
{
    if(particleGun)
    delete particleGun;
    delete Xene;
    delete YprobInteg;
}
void MicrobeamPrimaryGeneratorAction::GeneratePrimaries(G4Event*anEvent)
{
    // Define the primary particle type
    G4ParticleTable* particleTable =G4ParticleTable::GetParticleTable();
    G4String ParticleName = "gamma";
    G4ParticleDefinition* particle = particleTable->FindParticle(ParticleName);
    particleGun->SetParticleDefinition(particle);
    static int times=0;
    times++;
    if( times%100 ==0 ) G4cout << " N particles generated = " << times << G4endl;
    G4double x,y,z,a,b,c;
    G4double xs, ys;
    while (1){
    xs=0.0125*mm;
    ys=5*mm;
    x=xs*(2*G4UniformRand()-1)*mm;
    y=ys*(2*G4UniformRand()-1)*mm;
    z=-sizeZ;
    break;
}
G4ThreeVector position(x,y,z);
particleGun->SetParticlePosition(position);
// generation of the impulse direction of primary particles
a= 0.0;
b= 0.0;
c= 1.0;
G4ThreeVector direction(a,b,c);
particleGun->SetParticleMomentumDirection(direction);
    if(spectrumChoice==0 )
    {
        G4double random = G4UniformRand()*YprobInteg[NbeamEne-1];
        G4double interpola(G4int N, G4double *X, G4double *F,G4double x);
        primaryParticleEnergy = keV*interpola(NbeamEne,YprobInteg,Xene,
        random);
    }
    else
    {
        primaryParticleEnergy = MonoEnergy ;
    }
    particleGun->SetParticleEnergy(primaryParticleEnergy);
    // generate primary particle
    particleGun->GeneratePrimaryVertex(anEvent);
}
G4double MicrobeamPrimaryGeneratorAction::GetEnergy()
{
    return primaryParticleEnergy;
}
G4double interpola(G4int N, G4double *X, G4double *F,G4double x)
{
    G4int l,h,m;
    l=-1;
    h= N;
    while(1)
    {
        m=(l+h)>>1;
        if(X[m]>x)
        {
            h=m;
        }
        else
        {
            l=m;
        }
        if( (h-l)<2 ) break;
        }
    if(X[m]==x) return F[m];
    if(l==-1 || h==N) return 0;
    return ( F[h]*(x-X[l]) + F[l]*(X[h]-x))/(X[h]-X[l]);
}
