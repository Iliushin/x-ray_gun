#include "MicrobeamFactory.hh"
#include "MicrobeamPrimaryGeneratorAction.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "G4ParticleTable.hh"
#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4IonTable.hh"
#include "G4UImanager.hh"
#include "globals.hh"
#include "G4RunManager.hh"


MicrobeamFactory:: MicrobeamFactory()
{
    MicrobeamPrimaryParticle=NULL;
}

MicrobeamFactory:: MicrobeamFactory(MicrobeamDetectorConstruction*detector)
{
    sizeZ = detector->GetBoxDim_Z();
    MicrobeamPrimaryParticle = new MicrobeamPrimaryGeneratorAction(sizeZ);
    monoEnergy=-1;
}
MicrobeamFactory::~MicrobeamFactory()
{
    delete MicrobeamPrimaryParticle;
}
G4VUserPrimaryGeneratorAction*MicrobeamFactory::CreatePrimaryGeneratorAction()
{
    G4cout << "create the source \n";
    return MicrobeamPrimaryParticle;
}
void MicrobeamFactory::CreateSource(G4VPhysicalVolume *)
{
}
void MicrobeamFactory::CleanSource()
{
}
void MicrobeamFactory::SelectMonoEnergy(G4double value)
{
monoEnergy = value;
}
