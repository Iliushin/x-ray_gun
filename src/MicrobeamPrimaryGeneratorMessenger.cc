#include "MicrobeamPrimaryGeneratorMessenger.hh"
#include "MicrobeamPrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
MicrobeamPrimaryGeneratorMessenger::MicrobeamPrimaryGeneratorMessenger
(MicrobeamPrimaryGeneratorAction* primary):primaryAction(primary)
{
    primaryDir =new G4UIdirectory("/primary/");
    primaryDir->SetGuidance("PrimaryGenerator control");
    primaryParticleMonoEnergySpectrumCmd =
    new G4UIcmdWithADoubleAndUnit("/primary/energy",this);
    primaryParticleMonoEnergySpectrumCmd->SetGuidance(" Microbeam with fixed energy : Esrf Source " );
    primaryParticleMonoEnergySpectrumCmd->
    SetParameterName("Energy",true,true);
    primaryParticleMonoEnergySpectrumCmd->
    AvailableForStates(G4State_PreInit,G4State_Idle);
    primaryParticleMonoEnergySpectrumCmd->SetDefaultUnit("keV");
    primaryParticleMonoEnergySpectrumCmd->SetUnitCandidates("eV keV");
}
MicrobeamPrimaryGeneratorMessenger::~MicrobeamPrimaryGeneratorMessenger()
{
    delete primaryParticleMonoEnergySpectrumCmd;
    delete primaryDir;
}
    void MicrobeamPrimaryGeneratorMessenger::SetNewValue(
    G4UIcommand* command, G4String newValue)
{
    if( command ==primaryParticleMonoEnergySpectrumCmd )
    {
        primaryAction->SelectMonoEnergy(primaryParticleMonoEnergySpectrumCmd ->GetNewDoubleValue(newValue));
    }
}




