#include "MicrobeamPhysicsList.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4UnitsTable.hh"
#include "G4ios.hh"
#include "G4SystemOfUnits.hh"
#include "G4LowEnergyRayleigh.hh"
#include "G4LowEnergyPhotoElectric.hh"
#include "G4LowEnergyCompton.hh"
#include "G4LowEnergyGammaConversion.hh"
#include "G4LowEnergyIonisation.hh"
#include "G4VProcess.hh"

MicrobeamPhysicsList::MicrobeamPhysicsList():G4VUserPhysicsList()
{
    defaultCutValue = 0.001*mm;
    cutForGamma = defaultCutValue;
    cutForElectron = defaultCutValue;
    cutForPositron = defaultCutValue;
    SetVerboseLevel(0);
}
MicrobeamPhysicsList::~MicrobeamPhysicsList()
{}
void MicrobeamPhysicsList::ConstructParticle()
{
//In this method, static member functions should be called
//for all particles which you want to use.
//This ensures that objects of these particle types will be
//created in the program.

    ConstructBosons();
    ConstructLeptons();
}
void MicrobeamPhysicsList::ConstructBosons()
{
    // gamma
    G4Gamma::GammaDefinition();
}
void MicrobeamPhysicsList::ConstructLeptons()
{
    // leptons
    G4Electron::ElectronDefinition();
    G4Positron::PositronDefinition();
}
    void MicrobeamPhysicsList::ConstructProcess()
{
    AddTransportation();
    ConstructEM();
}





    // e-

    #include "G4LowEnergyBremsstrahlung.hh"
    // e+
    #include "G4eIonisation.hh"
    #include "G4eBremsstrahlung.hh"
    #include "G4eplusAnnihilation.hh"

    //PENELOPE
    #include "G4MultipleScattering.hh"
    //gamma
    #include "G4PenelopeCompton.hh"
    #include "G4PenelopeGammaConversion.hh"
    #include "G4PenelopePhotoElectric.hh"
    #include "G4PenelopeRayleigh.hh"
    //e-
    #include "G4PenelopeIonisation.hh"
    #include "G4PenelopeBremsstrahlung.hh"
    #include "G4PenelopeAnnihilation.hh"
    #include "G4StepLimiter.hh"

void MicrobeamPhysicsList::ConstructEM()
{
    theParticleIterator->reset();
    while( (*theParticleIterator)() )
    {
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        G4String particleName = particle->GetParticleName();
        //processes Low energy
        if (particleName == "gamma")
        {
            //gamma
            pmanager->AddDiscreteProcess(new G4LowEnergyRayleigh);
            lowePhot = new G4LowEnergyPhotoElectric("LowEnPhotoElec");
            pmanager->AddDiscreteProcess(lowePhot);
            pmanager->AddDiscreteProcess(new G4LowEnergyCompton);
            pmanager->AddDiscreteProcess(new G4LowEnergyGammaConversion);
        }
        else if (particleName == "e-")
        {
            //electron
            loweIon = new G4LowEnergyIonisation("LowEnergyIoni");
            loweBrem = new G4LowEnergyBremsstrahlung("LowEnBrem");
            loweBrem->SetAngularGenerator("tsai");
            pmanager->AddProcess(new G4MultipleScattering, -1, 1,1);
            pmanager->AddProcess(loweIon, -1, 2, 2);
            pmanager->AddProcess(loweBrem, -1,-1,3);
        }
    }
}

    //processes PENELOPE

    if (particleName == "gamma")
    {
    pmanager->AddDiscreteProcess(new G4PenelopePhotoElectric);
    pmanager->AddDiscreteProcess(new G4PenelopeCompton);
    pmanager->AddDiscreteProcess(new G4PenelopeGammaConversion);
    pmanager->AddDiscreteProcess(new G4PenelopeRayleigh);
    pmanager->AddProcess(new G4StepLimiter(),-1,-1, 3);
    }
    else if (particleName == "e-")
    {
    G4PenelopeIonisation* ioni = new G4PenelopeIonisation();
    G4PenelopeBremsstrahlung* brem = new
    G4PenelopeBremsstrahlung();
    pmanager -> AddProcess(ioni, -1, 2,2);
    pmanager -> AddProcess(brem, -1,-1,3);
    pmanager->AddProcess(new G4MultipleScattering, -1, 1,1);
    pmanager->AddProcess(new G4StepLimiter(),-1,-1,3);
    }

void MicrobeamPhysicsList::SetCuts()
{
//special for low energy physics
G4ProductionCutsTable::GetProductionCutsTable() ->SetEnergyRange(250*eV, 100*GeV);
SetCutValue(cutForGamma, "gamma");
SetCutValue(cutForElectron, "e-");
if (verboseLevel>0) DumpCutValuesTable();
}
void MicrobeamPhysicsList::SetGammaLowLimit(G4double lowcut)
{
SetGELowLimit(lowcut);
}
void MicrobeamPhysicsList::SetElectronLowLimit(G4double lowcut)
{
SetGELowLimit(lowcut);
}
void MicrobeamPhysicsList::SetGELowLimit(G4double lowcut)
{
G4ProductionCutsTable::GetProductionCutsTable()->
SetEnergyRange(lowcut,1e5);
}
void MicrobeamPhysicsList::SetGammaCut(G4double val)
{
ResetCuts();
cutForGamma = val;
}
void MicrobeamPhysicsList::SetElectronCut(G4double val)
{
cutForElectron = val;
}
void MicrobeamPhysicsList::SetPositronCut(G4double val)
{
cutForPositron = val;
}
void MicrobeamPhysicsList::SetLowEnSecPhotCut(G4double cut)
{
G4cout<<"Low energy secondary photons cut is now set to: "<<cut*MeV<<" (MeV)"<<G4endl;
G4cout<<"for processes LowEnergyPhotoElectric,LowEnergyBremsstrahlung, LowEnergyIonisation"<<G4endl;
lowePhot->SetCutForLowEnSecPhotons(cut);
loweIon->SetCutForLowEnSecPhotons(cut);
loweBrem->SetCutForLowEnSecPhotons(cut);
}
void MicrobeamPhysicsList::SetLowEnSecElecCut(G4double cut)
{
G4cout<<"Low energy secondary electrons cut is now set to: "<<cut*MeV<<" (MeV)"<<G4endl;
G4cout<<"for processes LowEnergyIonisation"<<G4endl;
loweIon->SetCutForLowEnSecElectrons(cut);}

