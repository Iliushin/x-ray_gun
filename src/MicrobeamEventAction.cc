#include "MicrobeamEventAction.hh"
#include "MicrobeamPhantomHit.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "MicrobeamEventMessenger.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "G4VVisManager.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"


MicrobeamEventAction::MicrobeamEventAction(G4String &SDName) : drawFlag("all" )
{
    hitsCollectionID = -1;
    G4String sensitiveDetectorName = SDName;

    detector=new MicrobeamDetectorConstruction(sensitiveDetectorName);
    numberOfVoxelZ = detector->GetNumVoxelZ();
    numberOfVoxelY = detector->GetNumVoxelY();
    numberOfVoxelX = detector->GetNumVoxelX();
    voxelWidthX = detector->GetDimX() /numberOfVoxelX;
    voxelWidthY = detector->GetBoxDim_Y() /numberOfVoxelY;
    voxelWidthZ = detector->GetBoxDim_Z () /numberOfVoxelZ;

    storage = new float[numberOfVoxelX*numberOfVoxelY*numberOfVoxelZ];
    DoseStorage = new float[numberOfVoxelX*numberOfVoxelY*numberOfVoxelZ];
    eventMessenger = new MicrobeamEventMessenger(this);
}

void MicrobeamEventAction::dump(G4String * name)
    {
    FILE * f = fopen(name->data(),"w");
    G4int i;
    G4int j=0;
    G4int k;
        for (i=0 ; i< numberOfVoxelX ; i++ )
        {
            for(k=0 ; k <10 ; k++ )
            {
                fprintf(f,"x=%i z=%i\t %e\t",i,k,storage[(i*numberOfVoxelY+j)*numberOfVoxelZ+k]);
            }
            fprintf(f,"\n");
         }
    memset(storage,0, numberOfVoxelX*numberOfVoxelY*numberOfVoxelZ*sizeof(float));
    fclose(f);
    }
void MicrobeamEventAction::print(G4String * name)
    {
    FILE * fout = fopen(name->data(),"w");
    G4int i;
    G4int j=19;
    G4int k;
        for (i=0 ; i< numberOfVoxelX ; i++ )
        {
            for(k=0 ; k <10 ; k++ )
            {
            fprintf(fout, "x= %i z= %i\t %e\t ", i, k,  DoseStorage[(i*numberOfVoxelY+j)*numberOfVoxelZ+k]);
            }
            fprintf(fout,"\n");
         }
    memset(DoseStorage,0, numberOfVoxelX*numberOfVoxelY*numberOfVoxelZ*sizeof(float));
    fclose(fout);
    }

MicrobeamEventAction::~MicrobeamEventAction()
{
delete detector;
delete storage;
delete DoseStorage;
}

void MicrobeamEventAction::BeginOfEventAction(const G4Event*)
{
G4SDManager* sensitiveDetectorManager = G4SDManager::GetSDMpointer();
if(hitsCollectionID==-1)
    hitsCollectionID = sensitiveDetectorManager->GetCollectionID("PhantomHitsCollectionCili");
}

void MicrobeamEventAction::EndOfEventAction(const G4Event* evt)
{
    if(hitsCollectionID < 0)
    return;

    G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
    MicrobeamPhantomHitsCollection* CHC = NULL;

    if(HCE)
        CHC = (MicrobeamPhantomHitsCollection*)(HCE->GetHC(hitsCollectionID));

    if(CHC)
    {
        G4int hitCount = CHC->entries();
            for (G4int h = 0; h < hitCount; h++)
            {
                G4int i=((*CHC)[h])->GetXID();
                G4int j=((*CHC)[h])->GetYID();
                G4int k=((*CHC)[h])->GetZID();

                G4double EnergyDep=((*CHC)[h]->GetEdep());
                G4double DoseDep;
                G4double VoxelXThickness=0.001*mm;
                G4double VoxelYThickness=5*mm;
                G4double VoxelZThickness=10*mm;
                G4double Volume=VoxelXThickness*VoxelYThickness*VoxelZThickness/mm3;
            //if plexiglass
                DoseDep=(1.6021*1e-7*EnergyDep)/(Volume*1.19);

            // if water
            //DoseDep=(1.6021*1e-7*EnergyDep)/(Volume*1.0);

            storage[(i*numberOfVoxelY+j)*numberOfVoxelZ+k]+=EnergyDep/MeV;
                DoseStorage[(i*numberOfVoxelY+j)*numberOfVoxelZ+k]+=DoseDep;
            }
    }

    // extract the trajectories and draw them

if (G4VVisManager::GetConcreteInstance())
    {
    G4TrajectoryContainer * trajectoryContainer = evt->GetTrajectoryContainer();
    G4int n_trajectories = 0;
        if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();
            for (G4int i=0; i<n_trajectories; i++)
                {
                G4Trajectory* trj = (G4Trajectory*)((*(evt-> GetTrajectoryContainer()))[i]);
                    if(drawFlag == "all")
                    trj->DrawTrajectory();//was 50
                    else if((drawFlag == "charged")&&(trj->GetCharge() != 0.))
                    trj->DrawTrajectory();
                    else if ((drawFlag == "neutral")&&(trj->GetCharge() == 0.))
                    trj->DrawTrajectory();
                }
    }
}
