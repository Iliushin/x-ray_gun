#include "MicrobeamPhantomROGeometryCili.hh"
#include "MicrobeamDummySD.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SDManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"

MicrobeamPhantomROGeometryCili::MicrobeamPhantomROGeometryCili(G4String aString,
G4double phantomDimX,
G4double phantomDimY,
G4double phantomDimZ,
G4int numberOfVoxelsX,
G4int numberOfVoxelsY,
G4int numberOfVoxelsZ) : G4VReadOutGeometry(aString),
phantomOuterRadius(phantomDimX),
phantomOuterRadius2(phantomDimY),
phantomHeight(phantomDimZ),
numberOfVoxelsAlongX(numberOfVoxelsX),
numberOfVoxelsAlongY(numberOfVoxelsY),
numberOfVoxelsAlongZ(numberOfVoxelsZ),
dummySD(0)
MicrobeamPhantomROGeometryCili::~MicrobeamPhantomROGeometryCili()
{
    if(dummySD) delete dummySD;
    dummySD=0;
}
G4VPhysicalVolume* MicrobeamPhantomROGeometryCili::Build()
{
    G4Material* dummyMat = new
    G4Material(name="dummyMat",1,1*g/mole,1*g/cm3);
    G4double worldDimensionX = 40.0*cm;
    G4double worldDimensionY = 40.0*cm;
    G4double worldDimensionZ = 40.0*cm;
    G4double halfROPhantomSizeX = phantomOuterRadius/1000;
    G4double halfPhantomSizeX = halfROPhantomSizeX;
    G4double halfPhantomSizeY = phantomOuterRadius2;
    G4double halfPhantomSizeZ = phantomHeight/2;
    // world volume of ROGeometryCili ...
    G4Box *ROWorld = new G4Box("ROWorld", worldDimensionX,
    worldDimensionY, worldDimensionZ);
    G4LogicalVolume *ROWorldLog = new G4LogicalVolume(ROWorld, dummyMat,"ROWorldLog", 0,0,0);
    G4VPhysicalVolume *ROWorldPhys = new G4PVPlacement(0, G4ThreeVector(), "ROWorldPhys", ROWorldLog, 0, false, 0);
    // phantom ROGeometryCili definition
    G4Box * ROPhantom = new G4Box("ROPhantom", halfPhantomSizeX, halfPhantomSizeY, halfPhantomSizeZ);
    G4LogicalVolume *ROPhantomLog = new G4LogicalVolume(ROPhantom, dummyMat, "ROPhantomLog", 0,0,0);
    G4RotationMatrix yRot90deg;
    // Rotates X and Z axes only
    yRot90deg.rotateY(M_PI/2.*rad);
    G4VPhysicalVolume *ROPhantomPhys = new G4PVPlacement(0,
    G4ThreeVector(), "PhantomPhys", ROPhantomLog, ROWorldPhys, false,0);
    // Division along X axis: the phantom is divided in slices along the
    //X axis
    G4double halfXVoxelSizeX = halfPhantomSizeX/numberOfVoxelsAlongX;
    G4double halfXVoxelSizeY = halfPhantomSizeY;
    G4double halfXVoxelSizeZ = halfPhantomSizeZ;
    G4double voxelXThickness = 2*halfXVoxelSizeX;

    G4Box *ROPhantomXDivision = new G4Box("ROPhantomXDivision", halfXVoxelSizeX, halfXVoxelSizeY, halfXVoxelSizeZ);
    G4LogicalVolume *ROPhantomXDivisionLog = new G4LogicalVolume(ROPhantomXDivision,
                                                                 dummyMat,
                                                                 "ROPhantomXDivisionLog",
                                                                 0,0,0);

    G4VPhysicalVolume *ROPhantomXDivisionPhys = new  G4PVReplica("ROPhantomXDivisionPhys",
                                                                 ROPhantomXDivisionLog, ROPhantomPhys,
                                                                 kXAxis,numberOfVoxelsAlongX,voxelXThickness);

    //Y DIVISION
    G4double halfYVoxelSizeX = halfXVoxelSizeX;
    G4double halfYVoxelSizeY = halfPhantomSizeY/numberOfVoxelsAlongY;
    G4double halfYVoxelSizeZ = halfPhantomSizeZ;
    G4double voxelYThickness = 2*halfYVoxelSizeY;

    G4Box *ROPhantomYDivision = new G4Box("ROPhantomYDivision",
    halfYVoxelSizeX,
    halfYVoxelSizeY,
    halfYVoxelSizeZ);

    G4LogicalVolume *ROPhantomYDivisionLog = new G4LogicalVolume(ROPhantomYDivision, dummyMat,
                                                                 "ROPhantomYDivisionLog", 0,0,0);
    G4VPhysicalVolume *ROPhantomYDivisionPhys = new G4PVReplica("ROPhantomYDivisionPhys",
                                                                ROPhantomYDivisionLog,
                                                                ROPhantomXDivisionPhys,
                                                                kYAxis, numberOfVoxelsAlongY,
                                                                voxelYThickness);
    // Division along Z axis: the slices along the Y axis are divided
    //along the Z axis
    G4double halfZVoxelSizeX = halfXVoxelSizeX;
    G4double halfZVoxelSizeY = halfYVoxelSizeY;
    G4double halfZVoxelSizeZ = halfPhantomSizeZ/numberOfVoxelsAlongZ;
    G4double voxelZThickness = 2*halfZVoxelSizeZ;
    G4Box *ROPhantomZDivision = new G4Box("ROPhantomZDivision",
                                          halfZVoxelSizeX,
                                          halfZVoxelSizeY,
                                          halfZVoxelSizeZ);
    G4LogicalVolume *ROPhantomZDivisionLog = new G4LogicalVolume(ROPhantomZDivision,
                                                                 dummyMat,
                                                                 "ROPhantomZDivisionLog", 0,0,0);
    ROPhantomZDivisionPhys = new G4PVReplica("ROPhantomZDivisionPhys", ROPhantomZDivisionLog,
                                             ROPhantomYDivisionPhys, kZAxis,
                                             numberOfVoxelsAlongZ, voxelZThickness);
    MicrobeamDummySD *dummySD = new MicrobeamDummySD;
    ROPhantomZDivisionLog->SetSensitiveDetector(dummySD);
    return ROWorldPhys;
}



























