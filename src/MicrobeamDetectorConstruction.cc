#include "MicrobeamPhantomROGeometryCili.hh"
#include "MicrobeamPhantomSDCili.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "MicrobeamFactory.hh"
#include "G4CSGSolid.hh"
#include "G4Sphere.hh"
#include "G4MaterialPropertyVector.hh"
#include "G4SDManager.hh"
#include "G4SubtractionSolid.hh"
#include "G4RunManager.hh"
#include "G4MaterialPropertiesTable.hh"
#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4MaterialTable.hh"
#include "Randomize.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4PVParameterised.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4Colour.hh"
#include "G4UserLimits.hh"
#include "G4UnionSolid.hh"
#include "G4VisAttributes.hh"
#include "G4VSensitiveDetector.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

MicrobeamDetectorConstruction::MicrobeamDetectorConstruction(G4String &SDName): phantomSDCili(0), phantomROGeometry(0), factory(0),
World(0), WorldLog(0), WorldPhys(0),
Phantom(0), PhantomLog(0), PhantomPhys(0)
    {
        phantomInnerRadius= 0*cm ;
        phantomOuterRadius= 10*cm ;
        phantomOuterRadius2= 10*cm ;
        phantomHeight= 20*cm ;
        phantomStartAngle= 0*deg ;
        phantomSpanningAngle= 360*deg;

// Define the number of voxels in which the phantom is subdivided
//along the three axis

        numberOfVoxelsAlongZ=20;
        numberOfVoxelsAlongX=200;
        numberOfVoxelsAlongY=40;

// Define the sizes of the World volume contaning the phantom
    Worldx = 40.0*cm;
    Worldy = 40.0*cm;
    Worldz = 40.0*cm;

    sensitiveDetectorName = SDName;

    // Define the default source modelled in the geometry
     factory = new MicrobeamFactory();

    }

MicrobeamDetectorConstruction::~MicrobeamDetectorConstruction()
{
    delete factory;

    if (phantomROGeometry)
    delete phantomROGeometry;
}

G4VPhysicalVolume* MicrobeamDetectorConstruction::Construct()
{
         // Define required materials
                G4double A; // atomic mass
                G4double Z; // atomic number
                G4double d; // density

         // General elements
         A = 1.01*g/mole;
         G4Element* elH = new G4Element ("Hydrogen","H",Z = 1.,A);

         A = 14.01*g/mole;
         G4Element* elN = new G4Element("Nitrogen","N",Z = 7.,A);

         A = 16.00*g/mole;
         G4Element* elO = new G4Element("Oxygen","O",Z = 8.,A);

         A = 12.011*g/mole;
         G4Element* elC = new G4Element("Carbon","C",Z = 6.,A);

         // Perspex, plexiglass

         d = 1.19*g/cm3;
         G4Material*matplexiglass = new G4Material("Plexiglass",d,3);
            matplexiglass->AddElement(elH,0.08);
            matplexiglass->AddElement(elC,0.60);
            matplexiglass->AddElement(elO,0.32);

          // Air material
        d = 1.290*mg/cm3;
        G4Material* matAir = new G4Material("Air",d,2);
            matAir->AddElement(elN,0.7);
            matAir->AddElement(elO,0.3);

         // Water
        d = 1.000*g/cm3;
        G4Material*matH2O = new G4Material("Water",d,2);
            matH2O->AddElement(elH,2);
            matH2O->AddElement(elO,1);
            matH2O->GetIonisation()->SetMeanExcitationEnergy(75.0*eV);

            //@@@@@ construct phantom @@@@@

            // Define the colors
    G4Colour lblue (0.0,0.0,.75);
    G4Colour magenta (1.0,0.0,1.0);
    G4Colour red (1.0,0.0,0.0);
    G4Colour cyan (0.0,1.0,1.0);
    G4Colour green (0.0,1.0,0.0);

    // World volume
    World = new G4Box("World",Worldx,Worldy,Worldz);
    WorldLog = new G4LogicalVolume(World,matAir,"WorldLog",0,0,0);
    WorldPhys=new G4PVPlacement(0,G4ThreeVector(),"WorldPhys",WorldLog,0,false,0);

    // Plexiglass Cylinder
    Phantom = new G4Tubs("Phantom",phantomInnerRadius,phantomOuterRadius, phantomHeight/2, phantomStartAngle,
    phantomSpanningAngle);

    // Logical volume
    PhantomLog = new G4LogicalVolume(Phantom,matplexiglass,"PhantomLog",0,0,0);

    // Physical volume
    PhantomPhys = new G4PVPlacement(0,G4ThreeVector(),// Position:rotation and translation
                                       "PhantomPhys", // Name
                                          PhantomLog, // Associated logical volume
                                           WorldPhys, // Mother volume
                                     false,0);

    // Visualization attributes of the phantom
    G4VisAttributes* simpleBoxVisAtt= new G4VisAttributes(lblue);
    simpleBoxVisAtt->SetVisibility(true);
    simpleBoxVisAtt->SetForceWireframe(true);
    PhantomLog->SetVisAttributes(simpleBoxVisAtt);

    // Model the source in the phantom
    factory->CreateSource(PhantomPhys); //Build the source outside the phantom

    // Define the sensitive volume: phantom
    ConstructSensitiveDetectorCili();
    return WorldPhys;
}

void MicrobeamDetectorConstruction::ConstructSensitiveDetectorCili() // Sensitive Detector and ReadOut geometry definition
{

G4SDManager* pSDManager = G4SDManager::GetSDMpointer();
    if(!phantomSDCili)
    {
        phantomSDCili = new MicrobeamPhantomSDCili(sensitiveDetectorName+"Cili",
                                                   numberOfVoxelsAlongX,numberOfVoxelsAlongY,numberOfVoxelsAlongZ);

        G4String ROGeometryName = "PhantomROGeometryCili";
        phantomROGeometry = new MicrobeamPhantomROGeometryCili(ROGeometryName,phantomOuterRadius,
                                                               phantomOuterRadius2,
                                                               phantomHeight,
                                                               numberOfVoxelsAlongX,
                                                               numberOfVoxelsAlongY,
                                                               numberOfVoxelsAlongZ);
        phantomROGeometry->BuildROGeometry();
        phantomSDCili->SetROgeometry(phantomROGeometry);
        pSDManager->AddNewDetector(phantomSDCili);
        PhantomLog->SetSensitiveDetector( phantomSDCili );
    }
}
