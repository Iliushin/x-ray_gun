#include "MicrobeamEventMessenger.hh"
#include "MicrobeamEventAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

MicrobeamEventMessenger::MicrobeamEventMessenger(
MicrobeamEventAction* pMicrobeamEvent):eventManager(pMicrobeamEvent)
{
runDir = new G4UIdirectory("/event/");
runDir->SetGuidance("Summary of events.");

dumpDosesCmd = new G4UIcmdWithAString("/event/dump",this);
dumpDosesCmd->SetGuidance("Write data on a file");
dumpDosesCmd->AvailableForStates(G4State_Idle);

PrintDoseCmd = new G4UIcmdWithAString("/event/printdose",this);
PrintDoseCmd->SetGuidance("Write doses on a file");
PrintDoseCmd->AvailableForStates(G4State_Idle);
}
MicrobeamEventMessenger::~MicrobeamEventMessenger()
{
delete dumpDosesCmd;
delete PrintDoseCmd;
delete runDir;
}
void MicrobeamEventMessenger::SetNewValue(G4UIcommand*command,G4String newValue)
{
    if(command == dumpDosesCmd)
    {
        if (newValue != "") eventManager ->dump(&newValue);
    }
        if(command == PrintDoseCmd)
        {
        if (newValue != "") eventManager ->print(&newValue);
        }
}
