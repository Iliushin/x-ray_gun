#include "MicrobeamPhantomSDCili.hh"
#include "MicrobeamPhantomHit.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "G4Track.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Step.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4SDManager.hh"
#include "G4ParticleDefinition.hh"

MicrobeamPhantomSDCili::MicrobeamPhantomSDCili(G4String name,
                                               G4int NumVoxelX,
                                               G4int NumVoxelY,
                                               G4int NumVoxelZ)
: G4VSensitiveDetector(name), numberOfVoxelsX(NumVoxelX), numberOfVoxelsY(NumVoxelY), numberOfVoxelsZ(NumVoxelZ)
{
    G4String HCname;
    collectionName.insert(HCname = "PhantomHitsCollectionCili");
    voxelID = new G4int[numberOfVoxelsX * numberOfVoxelsY *
    numberOfVoxelsZ ];
    phantomHitsCollection = NULL;
}
MicrobeamPhantomSDCili::~MicrobeamPhantomSDCili()
{
    delete[] voxelID;
}
void MicrobeamPhantomSDCili::Initialize(G4HCofThisEvent*)
{
    phantomHitsCollection = new
    MicrobeamPhantomHitsCollection(SensitiveDetectorName,collectionName[0]);
    for(G4int i=0;i<numberOfVoxelsX;i++)
    for(G4int j=0;j<numberOfVoxelsY;j++)
    for(G4int k=0;k<numberOfVoxelsZ;k++)
    voxelID[(i*numberOfVoxelsY+j)*numberOfVoxelsZ+k] = -1;
}
G4bool MicrobeamPhantomSDCili::ProcessHits(G4Step* aStep,
G4TouchableHistory* ROhist)
{
    if(!ROhist) return false;
    if(aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName() != "PhantomPhys") return false;
    G4double energyDeposit = aStep->GetTotalEnergyDeposit();
    if(energyDeposit == 0.) return false;
    G4VPhysicalVolume* physVol = ROhist->GetVolume();
    // Read Voxel indexes: i is the x index, j is the y index, k is the
    //z index
    G4int k = ROhist->GetReplicaNumber(0);
    G4int i = ROhist->GetReplicaNumber(2);
    G4int j = ROhist->GetReplicaNumber(1);
    if(voxelID[(i*numberOfVoxelsY+j)*numberOfVoxelsZ+k] == -1)
    {
        MicrobeamPhantomHit* PhantomHit = new
        MicrobeamPhantomHit(physVol->GetLogicalVolume(),i,j,k);
        G4RotationMatrix rotM;
        if(physVol->GetObjectRotation()) rotM = *(physVol->GetObjectRotation());
        PhantomHit->SetEdep(energyDeposit);
        PhantomHit->SetPos(physVol->GetTranslation());
        PhantomHit->SetRot(rotM);
        G4int VoxelID = phantomHitsCollection->insert(PhantomHit);
        voxelID[(i*numberOfVoxelsY+j)*numberOfVoxelsZ+k]= VoxelID - 1;
    }
else
    (*phantomHitsCollection)
    [voxelID[(i*numberOfVoxelsY+j)*numberOfVoxelsZ+k] ] ->
    AddEdep(energyDeposit);
    return true;
}
void MicrobeamPhantomSDCili::EndOfEvent(G4HCofThisEvent*HCE)
{
    static G4int HCID = -1;
    if(HCID<0)
    {
        HCID = GetCollectionID(0);
    }
    HCE->AddHitsCollection(HCID,phantomHitsCollection);
}
