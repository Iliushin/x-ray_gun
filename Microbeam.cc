#include <time.h>
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
//??
#ifdef G4UI_USE_XM
#include "G4UIXm.hh"
#endif
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif
#include "MicrobeamEventAction.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "MicrobeamPhysicsList.hh"
#include "MicrobeamPhantomSDCili.hh"
#include "MicrobeamPrimaryGeneratorAction.hh"
#include "G4SDManager.hh"
#include "MicrobeamRunAction.hh"
#include "Randomize.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4UImessenger.hh"
#include "G4NistManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

int main(int argc ,char ** argv)
{

//choose the Random engine
CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
G4int seed = time(0);
CLHEP::HepRandom :: setTheSeed(seed);
G4RunManager* pRunManager = new G4RunManager;
G4String sensitiveDetectorName = "Phantom";
G4double sizeZ = 20*cm;

// Initialize the physics component
pRunManager->SetUserInitialization(new MicrobeamPhysicsList);

// Initialize the detector component
MicrobeamDetectorConstruction *pDetectorConstruction=new MicrobeamDetectorConstruction(sensitiveDetectorName);
pRunManager->SetUserInitialization(pDetectorConstruction);

// Initialize the primary particles
MicrobeamPrimaryGeneratorAction* primary = new MicrobeamPrimaryGeneratorAction(sizeZ);
pRunManager -> SetUserAction(primary);

// Initialize Optional User Action
MicrobeamEventAction *pEventAction=new MicrobeamEventAction(sensitiveDetectorName);
pRunManager->SetUserAction(pEventAction );

MicrobeamRunAction *pRunAction = new MicrobeamRunAction();
pRunManager->SetUserAction(pRunAction);
// Initialize the Visualization component

#ifdef G4VIS_USE
//Visualization manager
G4VisManager* visManager = new G4VisExecutive;
visManager->Initialize();
#endif

// Initialize the interactive session
G4UIsession* session = 0;
if (argc == 1)
// Define UI session for interactive mode.
{
session = new G4UIterminal();
}
//Initialize G4 kernel
pRunManager->Initialize();
// get the pointer to the User Interface manager
G4UImanager* UI = G4UImanager::GetUIpointer();
if (session)
// Define UI session for interactive mode.
{
G4cout<<" UI session starts ..."<< G4endl;
UI ->ApplyCommand("/contol/execute vis.mac");
session->SessionStart();
delete session;
}
else
// Batch mode
{
G4String command = "/control/execute ";
G4String fileName = argv[1];
UI->ApplyCommand(command+fileName);
}
// Job termination
#ifdef G4VIS_USE
delete visManager;
#endif
delete pRunManager;
return 0;
}
