#pragma once
#include "G4VSensitiveDetector.hh"
#include "MicrobeamPhantomHit.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;
class G4VSensitiveDetector;
class MicrobeamPhantomSDCili : public G4VSensitiveDetector
{
    public:
    MicrobeamPhantomSDCili(G4String name, G4int NumVoxelX,G4int NumVoxelY, G4int NumVoxelZ);
    ~MicrobeamPhantomSDCili();
    void Initialize(G4HCofThisEvent*);
    G4bool ProcessHits(G4Step*aStep,G4TouchableHistory*ROhist);
    void EndOfEvent(G4HCofThisEvent*HCE);
    private:
    const G4int numberOfVoxelsX;
    const G4int numberOfVoxelsY;
    const G4int numberOfVoxelsZ;
    G4int *voxelID;
    MicrobeamPhantomHitsCollection *phantomHitsCollection;
};

