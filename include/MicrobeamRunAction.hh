#pragma once
#include "G4UserRunAction.hh"
#include "G4RunManager.hh"
#include "globals.hh"

class G4Run;
class MicrobeamFactory;
class MicrobeamRunAction : public G4UserRunAction
{
    public:
    MicrobeamRunAction();
    ~MicrobeamRunAction();
    public:
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run* );
    void SelectMonoEnergy(G4double value);
    private:
    MicrobeamFactory *factory;
};

