#pragma once
#include "globals.hh"
 #include "G4VRestDiscreteProcess.hh"

 class G4PhysicsTableclass;
 class G4ParticleDefinition;
 class G4VParticleChange;
 class G4Track;
 class G4Step;
 class G4Material;

 class G4PenelopeAnnihilation : public G4VRestDiscreteProcess
  {
  public:

    G4PenelopeAnnihilation(const G4String& processName ="PenAnnih");

    ~G4PenelopeAnnihilation();

    G4bool IsApplicable(const G4ParticleDefinition&);

    void BuildPhysicsTable(const G4ParticleDefinition&);

    void PrintInfoDefinition();

    G4double DumpMeanFreePath(const G4Track& aTrack,
                              G4double previousStepSize,
                              G4ForceCondition* condition)
    { return GetMeanFreePath(aTrack, previousStepSize, condition); }

  protected:
    G4double GetMeanFreePath(const G4Track&,G4double,
                             G4ForceCondition*);

  public:

    G4VParticleChange* PostStepDoIt(const G4Track& aTrack,
                                     const G4Step& aStep);

    G4double GetMeanLifeTime(const G4Track& aTrack,
                            G4ForceCondition* condition);

    G4VParticleChange* AtRestDoIt(const G4Track& aTrack,
                                    const G4Step& aStep);

  private:

    G4double calculateCrossSectionPerElectron(G4double energy);


  private:

    // hide assignment operator as private
    G4PenelopeAnnihilation& operator=(const G4PenelopeAnnihilation& right);
    G4PenelopeAnnihilation(const G4PenelopeAnnihilation& );

  private:
    G4PhysicsTable* meanFreePathTable;

    G4double lowEnergyLimit;      // low  energy limit of the tables
    G4double highEnergyLimit;     // high energy limit of the tables
    G4int nBins;                  // number of bins in the tables
    G4double cutForLowEnergySecondaryPhotons;
 };
