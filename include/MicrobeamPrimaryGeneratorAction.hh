#pragma once
#include "globals.hh"
#include <vector>
#include "G4VUserPrimaryGeneratorAction.hh"
#include "MicrobeamPrimaryGeneratorMessenger.hh"

class G4ParticleGun;
class G4Run;
class G4Event;
class MicrobeamFactory;
class MicrobeamPrimaryGeneratorMessenger;
class MicrobeamPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
    public:
    MicrobeamPrimaryGeneratorAction(G4double sizeZ);
    ~MicrobeamPrimaryGeneratorAction();
    void SelectMonoEnergy(G4double value) ;
    void GeneratePrimaries(G4Event* anEvent);
    G4double GetEnergy();
    private:
    MicrobeamFactory* factory;
    G4ParticleGun* particleGun;
    G4double primaryParticleEnergy;
    G4double *Xene;
    G4double *YprobInteg;
    G4int NbeamEne;
    G4double sizeZ;
    static G4double MonoEnergy ;
    static int spectrumChoice;
    MicrobeamPrimaryGeneratorMessenger * primaryMessenger;
};
