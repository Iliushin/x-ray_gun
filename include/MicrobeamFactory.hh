#pragma once
#include "G4VUserPrimaryGeneratorAction.hh"
#include "MicrobeamDetectorConstruction.hh"
#include "G4RunManager.hh"

class G4ParticleGun;
class G4Run;
class G4Event;
class MicrobeamDetectorConstruction;
class MicrobeamFactory
{
    public:
    MicrobeamFactory();
    MicrobeamFactory(MicrobeamDetectorConstruction* detector);
    ~MicrobeamFactory();

    G4VUserPrimaryGeneratorAction* CreatePrimaryGeneratorAction();
    void CreatePrimaryGeneratorAction(G4Event* );
    void CreateSource(G4VPhysicalVolume*);
    void CleanSource();
    void SelectMonoEnergy(G4double value);

    private:
    G4double sizeZ;
    G4VUserPrimaryGeneratorAction * MicrobeamPrimaryParticle ;
    G4double monoEnergy;
};
