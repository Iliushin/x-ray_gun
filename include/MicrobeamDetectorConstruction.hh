#pragma once
#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4LogicalVolume.hh"
#include "G4VReadOutGeometry.hh"
#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4MaterialTable.hh"

//class MicrobeamDetectorMessenger;
class G4LogicalVolume;
class G4Material;
class G4Tubs;
class G4Box;
class G4Sphere;
class G4Colour;
class G4VPhysicalVolume;
class MicrobeamPhantomROGeometryCili;
class G4VSensitiveDetector;
class MicrobeamFactory;
//class G4Tubs;
class MicrobeamVoxelParameterisation;
class MicrobeamDetectorConstruction:public G4VUserDetectorConstruction
{
    public:
        MicrobeamDetectorConstruction(G4String&);
        ~MicrobeamDetectorConstruction();
        G4VPhysicalVolume*Construct();
        void ConstructSensitiveDetectorCili();
        const G4double VoxelWidth_X(){return phantomOuterRadius /numberOfVoxelsAlongX;}
        const G4double VoxelWidth_Y(){return phantomOuterRadius/numberOfVoxelsAlongY;}
        const G4double VoxelWidth_Z(){return phantomHeight/numberOfVoxelsAlongZ;}
        const G4int GetNumVoxelX() {return numberOfVoxelsAlongX;}
        const G4int GetNumVoxelZ() {return numberOfVoxelsAlongZ;}
        const G4int GetNumVoxelY() {return numberOfVoxelsAlongY;}
        const G4double GetDimX() {return phantomOuterRadius;}
        const G4double GetBoxDim_Y() {return phantomOuterRadius2;}
        const G4double GetBoxDim_Z() {return phantomHeight;}
    private:

        G4VSensitiveDetector* phantomSDCili;//pointer to sensitive detector for Microbeam ( cilindrical geometry
        G4VReadOutGeometry* phantomROGeometry;//pointer to ROGeometry
        MicrobeamFactory* factory;
// World ...
        G4Box*World; //pointer to the solid World
        G4LogicalVolume*WorldLog; //pointer to the logical World
        G4VPhysicalVolume* WorldPhys; //pointer to the physical World
// Phantom ...
        G4Tubs*Phantom;  //pointer to solid phantom
        G4LogicalVolume*PhantomLog; //pointer to logic phantom
        G4VPhysicalVolume*PhantomPhys;//pointer to physical phantom

    G4double phantomInnerRadius; //Phantom Inner Radius
    G4double phantomOuterRadius; //Phantom Outer Radius
    G4double phantomOuterRadius2; //Phantom Outer Radius
    G4double phantomHeight; //Phantom Height
    G4double phantomStartAngle; //Phantom Start Angle
    G4double phantomSpanningAngle; //Phantom Spanning Angle

    G4int numberOfVoxelsAlongX; //Number of voxels along x axis
    G4int numberOfVoxelsAlongY; //Number of voxels along y axis
    G4int numberOfVoxelsAlongZ; //Number of voxels along z axis

    G4double Worldx ; //World XDimension
    G4double Worldy ; //World YDimension
    G4double Worldz ; //World XDimension

    G4String sensitiveDetectorName;
    G4Material* matplexiglass;
    G4Material* matAir;
    G4Material* matH2O;
};

