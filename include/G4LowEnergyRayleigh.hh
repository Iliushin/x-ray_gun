#pragma once
#include "globals.hh"
#include "G4VDiscreteProcess.hh"
    class G4Track;
    class G4Step;
    class G4ParticleDefinition;
    class G4VParticleChange;
    class G4RDVEMDataSet;
    class G4RDVCrossSectionHandler;

    class G4LowEnergyRayleigh : public G4VDiscreteProcess {

    public:

      G4LowEnergyRayleigh(const G4String& processName ="LowEnRayleigh");

      ~G4LowEnergyRayleigh();

      G4bool IsApplicable(const G4ParticleDefinition&);

      void BuildPhysicsTable(const G4ParticleDefinition& photon);

      G4VParticleChange* PostStepDoIt(const G4Track& aTrack, const G4Step& aStep);

      // For testing purpose only
      G4double DumpMeanFreePath(const G4Track& aTrack,
                    G4double previousStepSize,
                    G4ForceCondition* condition)
      { return GetMeanFreePath(aTrack, previousStepSize, condition); }

    protected:

      G4double GetMeanFreePath(const G4Track& aTrack,
                   G4double previousStepSize,
                   G4ForceCondition* condition);

    private:

      // Hide copy constructor and assignment operator as private
      G4LowEnergyRayleigh& operator=(const G4LowEnergyRayleigh &right);
      G4LowEnergyRayleigh(const G4LowEnergyRayleigh& );

      G4double lowEnergyLimit;  // low energy limit  applied to the process
      G4double highEnergyLimit; // high energy limit applied to the process

      G4RDVEMDataSet* meanFreePathTable;
      G4RDVEMDataSet* formFactorData;

      G4RDVCrossSectionHandler* crossSectionHandler;

     const G4double intrinsicLowEnergyLimit; // intrinsic validity range
     const G4double intrinsicHighEnergyLimit;

   };
