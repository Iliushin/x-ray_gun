#pragma once
#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"

class MicrobeamPhantomHit : public G4VHit
{
public:
    MicrobeamPhantomHit(G4LogicalVolume* ,G4int ,G4int ,G4int );
    ~MicrobeamPhantomHit();
    MicrobeamPhantomHit(const MicrobeamPhantomHit &right);
    const MicrobeamPhantomHit& operator = (const MicrobeamPhantomHit&right);

    int operator == (const MicrobeamPhantomHit &right) const;
    inline void *operator new(size_t);
    inline void operator delete(void *aHit);

private:
    const G4LogicalVolume* logicalVolume;// Logical volume where the hit happened
    G4int xHitPosition; // Hit x coordinate
    G4int zHitPosition; // Hit z coordinate
    G4int yHitPosition; // Hit y coordinate
    G4ThreeVector hitPosition; //position of the hit
    G4RotationMatrix rotation; // rotation of the logical volume
    G4double energyDeposit; // energy deposit associated with the hit

public:
        // Set Hit position
    inline void SetCellID(G4int XID,G4int YID,G4int ZID) {xHitPosition = XID; zHitPosition = ZID; yHitPosition = YID;}

    inline G4int GetXID(){return xHitPosition;}// Get hit x coordinate
    inline G4int GetZID(){return zHitPosition;}// Get hit z coordinate
    inline G4int GetYID(){return yHitPosition;}// Get hit y coordinate
    inline void SetEdep(G4double edep){energyDeposit = edep;} //Set hit energy deposit
    inline void AddEdep(G4double edep){energyDeposit += edep;}// Add energy deposit
    inline G4double GetEdep(){return energyDeposit;}// Get energy deposit
    inline void SetPos(G4ThreeVector xyz){hitPosition = xyz;}// Set hit position
    inline G4ThreeVector GetPos(){return hitPosition;} // Get hit position
    inline void SetRot(G4RotationMatrix rmat){rotation = rmat;}//set rotation
    inline G4RotationMatrix GetRot(){return rotation;} //get rotation

    // It returns the logical volume where the hit happened
    inline const G4LogicalVolume * GetLogicalVolume(){return logicalVolume;}
};

typedef G4THitsCollection<MicrobeamPhantomHit>MicrobeamPhantomHitsCollection;
extern G4Allocator<MicrobeamPhantomHit> MicrobeamPhantomHitAllocator;
inline void* MicrobeamPhantomHit::operator new(size_t)
    {
        void *aHit;
        aHit = (void *) MicrobeamPhantomHitAllocator.MallocSingle();
        return aHit;
    }

inline void MicrobeamPhantomHit::operator delete(void *aHit)
    {
    MicrobeamPhantomHitAllocator.FreeSingle((MicrobeamPhantomHit*)aHit);
    }
