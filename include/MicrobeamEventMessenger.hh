#pragma once
#include "globals.hh"
#include "G4UImessenger.hh"
class MicrobeamEventAction;
class G4UIdirectory;
class G4UIcmdWithAString;
class MicrobeamEventMessenger: public G4UImessenger
{
public:MicrobeamEventMessenger(MicrobeamEventAction* );
    ~MicrobeamEventMessenger();
    void SetNewValue(G4UIcommand*, G4String);
    private:
        G4UIdirectory* runDir;
        G4UIcmdWithAString* dumpDosesCmd;
        G4UIcmdWithAString* PrintDoseCmd;
        MicrobeamEventAction* eventManager;
};

