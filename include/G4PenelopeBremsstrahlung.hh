#pragma once
#include "G4eLowEnergyLoss.hh"
 #include "G4DataVector.hh"
 #include "globals.hh"
 #include "G4PenelopeBremsstrahlungAngular.hh"

 class G4Track;
 class G4Step;
 class G4ParticleDefinition;
 class G4VParticleChange;
 class G4VEMDataSet;
 class G4VEnergySpectrum;
 class G4VCrossSectionHandler;
 class G4PenelopeBremsstrahlung : public G4eLowEnergyLoss
 {
 public:

   G4PenelopeBremsstrahlung(const G4String& processName = "PenelopeBrem");

   ~G4PenelopeBremsstrahlung();

   G4bool IsApplicable(const G4ParticleDefinition&);

   void BuildPhysicsTable(const G4ParticleDefinition& particleType);

   G4VParticleChange* PostStepDoIt(const G4Track& track,
                                   const G4Step& step);

   void SetCutForLowEnSecPhotons(G4double cut);

   void PrintInfoDefinition();

   //For testing purpose only
   G4double DumpMeanFreePath(const G4Track& aTrack,
                             G4double previousStepSize,
                             G4ForceCondition* condition)
   { return GetMeanFreePath(aTrack, previousStepSize, condition); }

 protected:

   G4double GetMeanFreePath(const G4Track& track,
                            G4double previousStepSize,
                            G4ForceCondition* condition );

 private:

   // Hide copy constructor and assignment operator as private
   G4PenelopeBremsstrahlung(const G4PenelopeBremsstrahlung& );
   G4PenelopeBremsstrahlung& operator = (const G4PenelopeBremsstrahlung& right);

   void BuildLossTable(const G4ParticleDefinition& ParticleType);

   void LoadAngularData();

   G4VCrossSectionHandler* crossSectionHandler;
   G4VEMDataSet* theMeanFreePath;
   G4VEnergySpectrum* energySpectrum;

   // Map to the objects containing tha angular data
   std::map<G4int,G4PenelopeBremsstrahlungAngular*> *angularData;


   // Lower limit for generation of gamma in this model
   G4DataVector cutForSecondaryPhotons;
   G4double cutForPhotons;

 };
