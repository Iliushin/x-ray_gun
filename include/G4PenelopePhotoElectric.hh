#pragma once
#include "globals.hh"
 #include "G4VDiscreteProcess.hh"
 #include "G4AtomicDeexcitation.hh"

 class G4Track;
 class G4Step;
 class G4ParticleDefinition;
 class G4VParticleChange;
 class G4VEMDataSet;
 class G4VCrossSectionHandler;
 class G4VRangeTest;

 class G4PenelopePhotoElectric : public G4VDiscreteProcess {

 public:

   G4PenelopePhotoElectric(const G4String& processName ="PenPhotoElec");

   ~G4PenelopePhotoElectric();

   G4bool IsApplicable(const G4ParticleDefinition&);

   void BuildPhysicsTable(const G4ParticleDefinition& photon);

   G4VParticleChange* PostStepDoIt(const G4Track& aTrack, const G4Step& aStep);

   void SetCutForLowEnSecPhotons(G4double);

   void SetCutForLowEnSecElectrons(G4double);

   void ActivateAuger(G4bool);

   // For testing purpose only
   G4double DumpMeanFreePath(const G4Track& aTrack,
                             G4double previousStepSize,
                             G4ForceCondition* condition)
   { return GetMeanFreePath(aTrack, previousStepSize, condition); }

 protected:

   G4double GetMeanFreePath(const G4Track& aTrack,
                            G4double previousStepSize,
                            G4ForceCondition* condition);

 private:

  // Hide copy constructor and assignment operator as private
   G4PenelopePhotoElectric& operator=(const G4PenelopePhotoElectric& right);
   G4PenelopePhotoElectric(const G4PenelopePhotoElectric& );

   G4double SampleElectronDirection(G4double);

   G4double lowEnergyLimit;  // low energy limit  applied to the process
   G4double highEnergyLimit; // high energy limit applied to the process

   G4VEMDataSet* meanFreePathTable;

   G4VCrossSectionHandler* crossSectionHandler;
   G4VCrossSectionHandler* shellCrossSectionHandler;

   G4VRangeTest* rangeTest;

   const G4double intrinsicLowEnergyLimit; // intrinsic validity range
   const G4double intrinsicHighEnergyLimit;

   G4double cutForLowEnergySecondaryPhotons;
   G4double cutForLowEnergySecondaryElectrons;

   G4AtomicDeexcitation deexcitationManager;
 };
