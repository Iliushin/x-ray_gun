#pragma once
#include "G4VReadOutGeometry.hh"
class MicrobeamDummySD;
class MicrobeamPhantomROGeometryCili : public G4VReadOutGeometry
{
public:
MicrobeamPhantomROGeometryCili(G4String aString,
                               G4double phantomDimX,
                               G4double phantomDimY,
                               G4double phantomDimZ,
                               G4int numberOfVoxelsX,
                               G4int numberOfVoxelsY,
                               G4int numberOfVoxelsZ);
~MicrobeamPhantomROGeometryCili();
private:
    G4VPhysicalVolume* Build();
private:
    const G4double phantomOuterRadius;
    const G4double phantomOuterRadius2;
    const G4double phantomHeight;
    const G4int numberOfVoxelsAlongX;
    const G4int numberOfVoxelsAlongY;
    const G4int numberOfVoxelsAlongZ;
G4VPhysicalVolume *ROPhantomZDivisionPhys;
};
