#pragma once
#include "G4UserEventAction.hh"
#include "globals.hh"
class MicrobeamDetectorConstruction;
class MicrobeamEventMessenger;
class MicrobeamEventAction : public G4UserEventAction
{
public:
MicrobeamEventAction(G4String&);
~MicrobeamEventAction();
public:
void BeginOfEventAction(const G4Event*);
void EndOfEventAction(const G4Event*);
void dump(G4String *);
void print(G4String *);
private:
G4String drawFlag; //Visualisation flag
G4int hitsCollectionID;
MicrobeamDetectorConstruction* detector;

G4int numberOfVoxelX;
G4int numberOfVoxelY;
G4int numberOfVoxelZ;
G4double voxelWidthX;
G4double voxelWidthY;
G4double voxelWidthZ;
G4double totalEnergy;
G4double sum;
float * storage;
float * DoseStorage;
MicrobeamEventMessenger * eventMessenger;
};
