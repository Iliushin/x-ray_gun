#pragma once
#include "G4eLowEnergyLoss.hh"
   #include "G4DataVector.hh"

   class G4Track;
   class G4Step;
   class G4ParticleDefinition;
   class G4VParticleChange;
   class G4RDVEMDataSet;
   class G4RDVEnergySpectrum;
   class G4RDVCrossSectionHandler;
   class G4RDVBremAngularDistribution;

   class G4LowEnergyBremsstrahlung : public G4eLowEnergyLoss
   {
   public:

     G4LowEnergyBremsstrahlung(const G4String& processName = "LowEnBrem");


     //  G4LowEnergyBremsstrahlung(const G4String& processName = "LowEnBrem",
     //                G4RDVBremAngularDistribution* distribution = 0);

     ~G4LowEnergyBremsstrahlung();

     G4bool IsApplicable(const G4ParticleDefinition&);

     void BuildPhysicsTable(const G4ParticleDefinition& particleType);

     G4VParticleChange* PostStepDoIt(const G4Track& track,
                     const G4Step& step);

     void SetCutForLowEnSecPhotons(G4double cut);

     void SetAngularGenerator(G4RDVBremAngularDistribution* distribution);

     void SetAngularGenerator(const G4String& name);

     void PrintInfoDefinition();


   protected:

    G4double GetMeanFreePath(const G4Track& track,
                 G4double previousStepSize,
                 G4ForceCondition* condition );

  private:

    // Hide copy constructor and assignment operator as private
    G4LowEnergyBremsstrahlung(const G4LowEnergyBremsstrahlung& );
    G4LowEnergyBremsstrahlung& operator = (const G4LowEnergyBremsstrahlung& right);

    void BuildLossTable(const G4ParticleDefinition& ParticleType);

    G4RDVCrossSectionHandler* crossSectionHandler;
    G4RDVEMDataSet* theMeanFreePath;
    G4RDVEnergySpectrum* energySpectrum;
    G4DataVector  energyBins;
    G4RDVBremAngularDistribution* angularDistribution;
    G4RDVBremAngularDistribution* TsaiAngularDistribution;
    G4String generatorName;

    // Lower limit for generation of gamma in this model
    G4DataVector cutForSecondaryPhotons;
    G4double cutForPhotons;
  };
