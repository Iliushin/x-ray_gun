#pragma once
#include "G4eLowEnergyLoss.hh"
#include "G4RDAtomicDeexcitation.hh"

    class G4Track;
    class G4Step;
    class G4ParticleDefinition;
    class G4VParticleChange;
    class G4RDVDataSetAlgorithm;
    class G4ParticleChange;
    class G4RDVEnergySpectrum;
    class G4RDVCrossSectionHandler;
    class G4RDShellVacancy;
    class G4RDVEMDataSet;

    class G4LowEnergyIonisation : public G4eLowEnergyLoss
    {
    public:

      G4LowEnergyIonisation(const G4String& processName = "LowEnergyIoni");

      ~G4LowEnergyIonisation();

      G4bool IsApplicable(const G4ParticleDefinition&);

      void PrintInfoDefinition();

      void BuildPhysicsTable(const G4ParticleDefinition& ParticleType);

      G4VParticleChange* PostStepDoIt(const G4Track& track,
                      const G4Step& step);

      void SetCutForLowEnSecPhotons(G4double cut);

      void SetCutForLowEnSecElectrons(G4double cut);

      void ActivateAuger(G4bool val);

    protected:

      G4double GetMeanFreePath(const G4Track& track,
                   G4double previousStepSize,
                   G4ForceCondition* condition );

   protected:

     virtual std::vector<G4DynamicParticle*>* DeexciteAtom(const G4MaterialCutsCouple* couple,
                                 G4double incidentEnergy,
                                 G4double eLoss);

   private:

     // Hide copy constructor and assignment operator as private
     G4LowEnergyIonisation(const G4LowEnergyIonisation& );
     G4LowEnergyIonisation& operator = (const G4LowEnergyIonisation& right);

     void BuildLossTable(const G4ParticleDefinition& ParticleType);

     G4RDVCrossSectionHandler* crossSectionHandler;
     G4RDVEMDataSet* theMeanFreePath;
     G4RDVEnergySpectrum* energySpectrum;

     // Lower limit for generation of gamma in this model
     G4DataVector cutForDelta;
     G4double cutForPhotons;
     G4double cutForElectrons;
     G4RDAtomicDeexcitation deexcitationManager;
     G4RDShellVacancy* shellVacancy;

   };
