#pragma once
#include "globals.hh"
    #include "G4VDiscreteProcess.hh"
    #include "G4RDAtomicDeexcitation.hh"

    class G4Track;
    class G4Step;
    class G4ParticleDefinition;
    class G4VParticleChange;
    class G4RDVEMDataSet;
    class G4RDVCrossSectionHandler;
    class G4RDVRangeTest;
    class G4RDVPhotoElectricAngularDistribution;

    class G4LowEnergyPhotoElectric : public G4VDiscreteProcess {

    public:

      G4LowEnergyPhotoElectric(const G4String& processName ="LowEnPhotoElec");

      ~G4LowEnergyPhotoElectric();

      G4bool IsApplicable(const G4ParticleDefinition&);

      void BuildPhysicsTable(const G4ParticleDefinition& photon);

      G4VParticleChange* PostStepDoIt(const G4Track& aTrack, const G4Step& aStep);

      void SetCutForLowEnSecPhotons(G4double);

      void SetCutForLowEnSecElectrons(G4double);

      void ActivateAuger(G4bool);

      void SetAngularGenerator(G4RDVPhotoElectricAngularDistribution* distribution);

      void SetAngularGenerator(const G4String& name);


      // For testing purpose only
      G4double DumpMeanFreePath(const G4Track& aTrack,
                    G4double previousStepSize,
                    G4ForceCondition* condition)
      { return GetMeanFreePath(aTrack, previousStepSize, condition); }

    protected:

      G4double GetMeanFreePath(const G4Track& aTrack,
                   G4double previousStepSize,
                  G4ForceCondition* condition);

   private:

    // Hide copy constructor and assignment operator as private
     G4LowEnergyPhotoElectric& operator=(const G4LowEnergyPhotoElectric& right);
     G4LowEnergyPhotoElectric(const G4LowEnergyPhotoElectric& );

     G4double lowEnergyLimit;  // low energy limit  applied to the process
     G4double highEnergyLimit; // high energy limit applied to the process

     G4RDVEMDataSet* meanFreePathTable;

     G4RDVCrossSectionHandler* crossSectionHandler;
     G4RDVCrossSectionHandler* shellCrossSectionHandler;

     G4RDVRangeTest* rangeTest;

     const G4double intrinsicLowEnergyLimit; // intrinsic validity range
     const G4double intrinsicHighEnergyLimit;

     G4double cutForLowEnergySecondaryPhotons;
     G4double cutForLowEnergySecondaryElectrons;

     G4RDAtomicDeexcitation deexcitationManager;

     G4RDVPhotoElectricAngularDistribution* ElectronAngularGenerator;
     G4String generatorName;
   };
