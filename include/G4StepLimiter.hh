#pragma once
#include "G4ios.hh"
    #include "globals.hh"
    #include "G4VProcess.hh"

    class G4StepLimiter : public G4VProcess
    {
      public:  // with description

         G4StepLimiter(const G4String& processName ="StepLimiter" );

         virtual ~G4StepLimiter();

         virtual G4double PostStepGetPhysicalInteractionLength(
                                 const G4Track& track,
                                 G4double   previousStepSize,
                                 G4ForceCondition* condition
                                );

         virtual G4VParticleChange* PostStepDoIt(
                                 const G4Track& ,
                                 const G4Step&
                                );

      public:  // without description

         //  no operation in  AtRestGPIL
         virtual G4double AtRestGetPhysicalInteractionLength(
                                 const G4Track& ,
                                 G4ForceCondition*
                                ){ return -1.0; };

         //  no operation in  AtRestDoIt
         virtual G4VParticleChange* AtRestDoIt(
                                 const G4Track& ,
                                 const G4Step&
                                ){return 0;};

         //  no operation in  AlongStepGPIL
         virtual G4double AlongStepGetPhysicalInteractionLength(
                                 const G4Track&,
                                 G4double  ,
                                 G4double  ,
                                 G4double& ,
                                 G4GPILSelection*
                                ){ return -1.0; };

         //  no operation in  AlongStepDoIt
         virtual G4VParticleChange* AlongStepDoIt(
                                 const G4Track& ,
                                 const G4Step&
                                ) {return 0;};

      private:

      // hide assignment operator as private
          G4StepLimiter(G4StepLimiter&);
          G4StepLimiter& operator=(const G4StepLimiter& right);

    };
