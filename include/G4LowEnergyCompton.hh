#pragma once
#include "globals.hh"
    #include "G4VDiscreteProcess.hh"
    #include "G4RDShellData.hh"
    #include "G4RDDopplerProfile.hh"

    class G4Track;
    class G4Step;
    class G4ParticleDefinition;
    class G4VParticleChange;
    class G4RDVEMDataSet;
    class G4RDVCrossSectionHandler;
    class G4RDVRangeTest;

    class G4LowEnergyCompton : public G4VDiscreteProcess {

    public:

      G4LowEnergyCompton(const G4String& processName ="LowEnCompton");

      ~G4LowEnergyCompton();

      G4bool IsApplicable(const G4ParticleDefinition& definition);

      void BuildPhysicsTable(const G4ParticleDefinition& definition);

      G4VParticleChange* PostStepDoIt(const G4Track& track, const G4Step& step);

      // For testing purpose only
      G4double DumpMeanFreePath(const G4Track& track,
                    G4double previousStepSize,
                    G4ForceCondition* condition)
      { return GetMeanFreePath(track, previousStepSize, condition); }

    protected:

      G4double GetMeanFreePath(const G4Track& track,
                   G4double previousStepSize,
                   G4ForceCondition* condition);

    private:

      // Hide copy constructor and assignment operator as private
      G4LowEnergyCompton& operator=(const G4LowEnergyCompton& right);
      G4LowEnergyCompton(const G4LowEnergyCompton& );

      G4double lowEnergyLimit;  // low energy limit  applied to the process
      G4double highEnergyLimit; // high energy limit applied to the process

      G4RDVEMDataSet* meanFreePathTable;
      G4RDVEMDataSet* scatterFunctionData;

      G4RDVCrossSectionHandler* crossSectionHandler;

     G4RDVRangeTest* rangeTest;

     const G4double intrinsicLowEnergyLimit; // intrinsic validity range
     const G4double intrinsicHighEnergyLimit;

     G4RDShellData shellData;
     G4RDDopplerProfile profileData;
   };
