#pragma once
#include "G4UImessenger.hh"
#include "globals.hh"

class MicrobeamPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class MicrobeamPrimaryGeneratorMessenger: public G4UImessenger
{
    public:
    MicrobeamPrimaryGeneratorMessenger(MicrobeamPrimaryGeneratorAction*);
    ~MicrobeamPrimaryGeneratorMessenger();
    void SetNewValue(G4UIcommand*, G4String);
    private:
    MicrobeamPrimaryGeneratorAction* primaryAction;
    G4UIdirectory*primaryDir;
    G4UIcmdWithADoubleAndUnit*primaryParticleMonoEnergySpectrumCmd;
};

